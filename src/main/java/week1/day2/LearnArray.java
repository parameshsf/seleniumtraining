package week1.day2;

import java.util.Scanner;

public class LearnArray {

	//Static Array
	public void staticArray() {
		int salary[] = {10000,12000,15000};
		System.out.println("Salary detail :"+salary[0]);
		
		//display all the array values using for loop
		for(int i=0;i<salary.length;i++) {
			System.out.println("Salary detail :"+salary[i]);
		}
		
	}
	//Dynamic Array
	public void dynamicArray() {
		//Runtime variable declaration
		Scanner dValue = new Scanner(System.in);
		System.out.println("Enter the size of an Array :");
		int size = dValue.nextInt();
		int salary[] = new int[size]; //Assigning size to an Array
		//Declaring size of an array
		for (int i=0;i<salary.length;i++) {
			salary[i]=dValue.nextInt(); //Assigning values to an Array during run time
		}
		for (int i=0;i<salary.length;i++) {
			System.out.println("Array Values :"+salary[i]); //Displaying array value
		}
		//For Each
		for (int salaryDet : salary) { 
			System.out.println("For Each Loop");
			System.out.println("Array Values :"+salaryDet); //Displaying array value
		}
		dValue.close();
		}
		
		public void dispOdd() {
			Scanner sOdd = new Scanner(System.in);
			System.out.println("Enter the size of an array");
			int arrSize = sOdd.nextInt();
			int oddArr[] = new int[arrSize];
			System.out.println("Enter the values to sum the odd numbers");
			for(int i=0;i<arrSize;i++) {
				oddArr[i]=sOdd.nextInt();
			}
			int sum = 0;
			for (int odd : oddArr) {
				if(odd%2!=0) {
				    sum = sum + odd;
				}
			}
			System.out.println("The sum of odd numbers are :" +sum);
			sOdd.close();
		}
		
			
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Static Array Initialization 
		
		LearnArray arr = new LearnArray();
		//arr.staticArray();
		//arr.dynamicArray();
		arr.dispOdd();
		
		
		

	}

}
