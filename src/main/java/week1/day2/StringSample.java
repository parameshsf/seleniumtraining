package week1.day2;

import java.util.Scanner;

public class StringSample {

	//String Verification
	
	
	public void strSample() {
		
		String nameList =  "Paramesh,Nandhini,Abhi,Babu";
		String ch[] = nameList.split(",");
		System.out.println("The names starts with 'A' and contains 'a'are dislpayed below"+"\n");
		for (String n : ch)
		{
			//System.err.println("enter"+n);
			
			if(n.startsWith("A") || n.contains("h"))
			{
				System.out.println(n);
				
			}
			
		}
		
		//Find the count of a or A in the given string
		Scanner findL = new Scanner(System.in);
		System.out.println("Enter the String to be search for a particular letter:");
		String s = findL.next();
		System.out.println("What letter you would like to count in the given above string?");
		//String findA = "Amazon India Private Limited";
		char f = findL.next().charAt(0);
		int count = 0;
		/*for(int i=0;i<findA.length();i++)
		{
			if(findA.charAt(i)=='a'||findA.charAt(i)=='A') {
				count = count + 1;
			}
		}*/
		for(int i=0;i<s.length();i++)
		{
			if(s.charAt(i)==f)
			{
				count = count + 1;
			}
		}
		System.out.println("The Count of the letter "+f+" in the given string is: "+"\n"+count);
		
		//Find the odd index of a string and convert it to upper case.
		
		String oddS = "testleaf";
		char chr[] = oddS.toCharArray();
		
		for(int i=0;i<oddS.length();i++)
		{
			char ch1 = oddS.charAt(i);
			
			if(i%2==0)
			{
				System.out.println(Character.toLowerCase(ch1));
			    chr[i] = Character.toLowerCase(ch1);
				//System.out.println("chr[i] is: "+chr[i]);
			}
			else
			{
				System.out.println(Character.toUpperCase(ch1));
				chr[i] = Character.toUpperCase(ch1);
				//System.out.println("chr[i] is: "+chr[i]);
			}
						
		}
		System.out.println(chr);
	
			/*for (char c : chr) {
			
			if(c%2==0)
			{
				System.out.println("Equals to 0");
				System.out.println(Character.toLowerCase(c));
							
			}
			else 
			{
				System.out.println("Not equals to 0");
				System.out.println(Character.toUpperCase(c));
			}
			
			
			}*/
	}
	
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
			StringSample str = new StringSample();
			str.strSample();
			
			
			
	}

}
