package week1.day1;

public class SmartTv {
	
	public String tvName = "LG";
	public short yearMfg = 2018;
	public long modelNo = 346789999;
	
	
	
	public void playGames(String gameName) {
		System.out.println("Play a Game "+gameName);
	}
	public void onTheTV() {
		System.out.println("TV is on");
	}
	public void dispTVDetails() {
		System.out.println("TV Details are :"+tvName +  "," +" Manufucturing Year: "+yearMfg +"," + " Model No: "+modelNo);
	}
	public void passVariables(String name, short mfg, long model) {
		
		tvName = name;
		yearMfg = mfg;
		modelNo = model;
		System.out.println("TV Details are :"+tvName +  "," +" Manufucturing Year: "+yearMfg +"," + " Model No: "+modelNo);
		
	}

}
