package week1.day1;

import java.util.Scanner;

public class MobilePhoneHW {
	
	public void sendSMS(String msg)
	{
		//@SuppressWarnings("resource")
		Scanner inputNo = new Scanner(System.in);
		System.out.println("Enter the phone number to send SMS :");
		long phoneNum = inputNo.nextLong();
		System.out.println(msg + " SMS sent to the phone number " + phoneNum);
		inputNo.close();
	}
	public void callMob(String msg1)
	{
		Scanner inputNo = new Scanner(System.in);
		System.out.println("Enter the phone number to make a call :");
		long phoneNum = inputNo.nextLong();
		System.out.println(msg1 + phoneNum);
		inputNo.close();
	}
	
	
	public static void main(String[] args) {
		MobilePhoneHW mob = new MobilePhoneHW();
		System.out.println("Your call balance amount is Rs.9, SMS Cost is Rs.1 per SMS, Call host is Rs.2 per call");
	
		/*for(int i=0;i<6;i++) {
			mob.sendSMS("Good Morning! Happy Sunday!");
			
		}*/
		for(int callBal=9;callBal>0;) {
			mob.sendSMS("Good Morning! Happy Sunday!");
			callBal = callBal - 1;
			System.out.println("Phone Balance after sending SMS is "+callBal);
			mob.callMob("The number you have dialled is ");
			callBal = callBal - 2;
			System.out.println("Phone Balance after making a call is "+callBal); 
		
		}
	}

}

