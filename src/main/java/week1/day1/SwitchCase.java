package week1.day1;

import java.util.Scanner;

public class SwitchCase {
	
	public void switchExample()
	{
		int value1, value2;
		String option;
		
		System.out.println("User will be asked to enter 2 values to perform arithmatic operations");
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the first value :" + "\n");
	    value1 = input.nextInt();
	    System.out.println("Enter the second value :" + "\n");
	    value2 = input.nextInt();
	    System.out.println("Enter either one of the options add or sub or mul or div to perform :" + "\n");
	    option = input.next();
	    
	    switch (option) {
		case "add":
			System.out.println("The added values are :"+(value1 + value2));
			break;
		case "sub":
			System.out.println("The subtracted values are :"+(value1 - value2));
			break;
		case "mul":
			System.out.println("The multiplied values are :"+(value1*value2));
			break;
		case "div":
			System.out.println("The divided values are :"+(value1/value2));
			break;
		default:
			System.out.println("Invalid Input Given");
			break;
		}
	    input.close();
	}
	
	public static void main(String[] args) {
		SwitchCase sc = new SwitchCase();
		sc.switchExample();
		
		
		
		//For Loop Example - Print Only the Even number
		
		for(int i=0;i<100;i++)
		{
			
			//i = i+1;
			//System.out.println("Value of i :"+(i+1));
			
			if(i%2==0)
			{
				System.out.println("Value of i is in even numbers :"+ i);
			}
				
		}
		
		
		
		
		
		
		
		
		
	}

}
