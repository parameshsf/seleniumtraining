package week1.day1;

import java.util.Scanner;

public class findBiggNumber {
	
	// THis class is to find the biggest of 3 numbers
	
	public static void main(String[] args) {
		int value1, value2, value3;
		
		System.out.println("User will be asked to enter 3 values to find the biggest of 3");
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the first value :" + "\n");
	    value1 = input.nextInt();
	    System.out.println("Enter the second value :" + "\n");
	    value2 = input.nextInt();
	    System.out.println("Enter the third value :" + "\n");
	    value3 = input.nextInt();

		if(value1 > value2 && value1 > value3 )
		{
			System.out.println("Value 1 is bigger");
		}
		else if(value2 > value3){
			System.out.println("Value 2 is bigger");
		}
		else
			System.out.println("Value 3 is bigger");
		}
		
				
	}


