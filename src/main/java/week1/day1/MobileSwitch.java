package week1.day1;

import java.util.Scanner;

public class MobileSwitch {
	
	public void dispCarrier()
	{
		String mob1 = "BSNL", mob2="Airtel", mob3="Jio";
		int option;
				
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the first 3 digits of your Mobile Number Please :");	    
	    option = input.nextInt();
	    
	    switch (option) {
		case 944:
			System.out.println("The Entered First 3 digit mobile number belongs to "+mob1+" Carrier");
			break;
		case 900:
			System.out.println("The Entered First 3 digit mobile number belongs to "+mob2+" Carrier");
			break;
		case 630:
			System.out.println("The Entered First 3 digit mobile number belongs to "+mob3+" Carrier");
			break;
		default:
			System.out.println("Invalid Input Given");
			break;
		}

	}
	
	public static void main(String[] args) {
		MobileSwitch sc = new MobileSwitch();
		sc.dispCarrier();
		
		
		//For Loop Example - Print Only the Even number
		
		/*for(int i=0;i<100;i++)
		{
			
			//i = i+1;
			//System.out.println("Value of i :"+(i+1));
			
			if(i%2==0)
			{
				System.out.println("Value of i is in even numbers :"+ i);
			}
				
		}*/
		
		
	
	}

}
