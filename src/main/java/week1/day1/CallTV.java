package week1.day1;

public class CallTV {
public static void main(String[] args) {
	SmartTv tv = new SmartTv(); //Creating a Class Object to call out methods and variables from another class
	tv.onTheTV();
	tv.dispTVDetails();
	tv.playGames("DeathRace");
	tv.passVariables("Sony", (short) 2016, 434341243L); //TypeCast is required when passing value in short data type, letter L required when passing long value as an argument
	
}
}
