package week1.day1;

import java.util.Scanner;

public class PhonePrice {
	
	// THis class is to find the costliest of 3 phones
	
	public static void main(String[] args) {
		int ph1, ph2, ph3;
		
		System.out.println("User will be asked to enter 3 phone prices to find the costliest of 3");
		
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the first phone price :" + "\n");
	    ph1 = input.nextInt();
	    System.out.println("Enter the second phone price :" + "\n");
	    ph2 = input.nextInt();
	    System.out.println("Enter the third phone price :" + "\n");
	    ph3 = input.nextInt();
	    input.close();
	    
		if(ph1 > ph2 || ph1 > ph3 )
		{
			System.out.println("Phone 1 is the costliest among 3 phones");
		}
		else if(ph2 > ph3){
			System.out.println("Phone 2 is the costliest among 3 phones");
		}
		else
			System.out.println("Phone 3 is the costliest among 3 phones");
		}
		
				
	}


