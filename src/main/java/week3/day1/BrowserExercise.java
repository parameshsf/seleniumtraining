package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BrowserExercise {
	
	public void browser() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		
		/*List<WebElement> findTag = driver.findElementsByTagName("a");	
		int tagCount = findTag.size();
		System.out.println(tagCount);
		findTag.get(27).click();*/
		String pwd = "Testleaf15";
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("Parameshsf");
		driver.findElementByLinkText("Check Availability").click();
		Thread.sleep(3000);
		//Use thread
				String chkAvail = driver.findElementById("userRegistrationForm:useravail").getText();
				System.out.println("Check Availability :"+chkAvail);
				if(chkAvail.contains("User Id is Available")) {
					System.out.println("User ID is not exists. Please proceed with registration");
				}
				else
				{
					System.out.println("User ID is already exists. Please enter different User Name");
				}
		driver.findElementById("userRegistrationForm:password").sendKeys(pwd);
		driver.findElementById("userRegistrationForm:confpasword").sendKeys(pwd);
		
		WebElement secQstn = driver.findElementById("userRegistrationForm:securityQ");
		Select sec = new Select(secQstn);
		sec.selectByVisibleText("Who was your Childhood hero?");
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("RajiniKanth");
		
		WebElement prefLang = driver.findElementById("userRegistrationForm:prelan");
		Select prefLan = new Select(prefLang);
		if(prefLan.getFirstSelectedOption().isSelected())
		{
			 System.out.println("Preferred Language is selected");
		}
		else
		{
			System.out.println("Preferred Language is Not selected");
		}
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Parameswaran");
		driver.findElementById("userRegistrationForm:gender:0").click();
		driver.findElementById("userRegistrationForm:maritalStatus:0").click();
		
		WebElement date = driver.findElementById("userRegistrationForm:dobDay");
		Select selDate = new Select(date);
		selDate.selectByVisibleText("23");
		
		WebElement month = driver.findElementById("userRegistrationForm:dobMonth");
		Select selMonth = new Select(month);
		selMonth.selectByIndex(0);
		
		WebElement year = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select yob = new Select(year);
		yob.selectByValue("1982");
		
		WebElement occp = driver.findElementById("userRegistrationForm:occupation");
		Select occup = new Select(occp);
		occup.selectByVisibleText("Professional");
		
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select selCountry = new Select(country);
		selCountry.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:email").sendKeys("parameshsf@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9952022192");
		
		WebElement natnlty = driver.findElementById("userRegistrationForm:nationalityId");
		Select selNationality = new Select(natnlty);
		selNationality.selectByVisibleText("India");
		
		driver.findElementById("userRegistrationForm:address").sendKeys("624");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("600127",Keys.TAB);
		Thread.sleep(4000);
		if(driver.findElementById("userRegistrationForm:statesName").getText().isEmpty()) {
			//driver.switchTo().alert().dismiss();
			System.out.println("State value is populated as expected");
		}
		else
		{
			//driver.switchTo().alert().accept();
			System.out.println("State value is NOT populated as expected");
		}
		
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select selCity = new Select(city);
		selCity.selectByIndex(1);
		
		Thread.sleep(4000);
		
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select postOffice = new Select(post);
		//postOffice.selectByVisibleText("Melakkottaiyur S.O");
		postOffice.selectByIndex(1);
		
		driver.findElementById("userRegistrationForm:landline").sendKeys("0446449142");
		driver.findElementByLinkText("Submit Registration Form>>>").click();
		driver.switchTo().alert().accept();
		driver.switchTo().alert().accept();
		driver.findElementById("userRegistrationForm:saveBtnIe").click();
		
		//Use thread
		//String chkAvail = driver.findElementById("userRegistrationForm:useravail").getText();
		//System.out.println("Check Availability :"+chkAvail);
		/*if(chkAvail.contains("User Id is Available")) {
			System.out.println("User ID is not exists. Please proceed with registration");
		}
		else
		{
			System.out.println("User ID is already exists. Please enter different User Name");
		}*/
	}
		
	
	
	public static void main(String[] args) throws InterruptedException {
		BrowserExercise bw = new BrowserExercise();
		bw.browser();
		
	}

}
