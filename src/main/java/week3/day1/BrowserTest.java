package week3.day1;

import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserTest {
	
	public void launchBrowser()
	{
		//System.setProperty("webdriver.chrome.driver", "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
		//System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("https://www.facebook.com");
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		//driver.quit();
	}
	public static void main(String[] args) {
		BrowserTest bt = new BrowserTest();
		bt.launchBrowser();
	}

}
