package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.internal.runners.model.EachTestNotifier;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class BrowserTest {
	
		
	public void launchBrowser() throws InterruptedException
	{
		//Setting up web driver and launching browser
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("http://www.leaftaps.com/opentaps");
		driver.manage().window().maximize();//Window Maximize
		
		//Login Page Key In Values
	
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		
		//Lead Page Key Inputs
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		driver.findElementById("createLeadForm_firstName").sendKeys("Parameswaran");
		driver.findElementById("createLeadForm_lastName").sendKeys("Sudarsanam");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Ramesh");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Ram");
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("140");
		
		//Working with Industry dropdown
		WebElement industryDD = driver.findElementById("createLeadForm_industryEnumId");
		Select industry = new Select(industryDD);
		industry.selectByValue("IND_SOFTWARE");
				
		List<WebElement> allValue = industry.getOptions();
		for (WebElement eachIndustry : allValue) {
			
			if(eachIndustry.getText().startsWith("M")) {
				System.out.println(eachIndustry.getText());
			}
		}
		
		//Working with Marketing Campaign Dropdown
		
		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mkt = new Select(marketing);
		int lastDropDown = mkt.getOptions().size();
		mkt.selectByIndex(lastDropDown-1);
		//mkt.selectByVisibleText("Automobile");
		
		
		//To list all dropdown values
		List<WebElement> lstMkt = mkt.getOptions();
		for (WebElement allMkt : lstMkt)
		{
			System.out.println(allMkt.getText());
		}
		
		//click link "Find Leads" using xPath
		
		//driver.findElementByXPath("//a[@id='ext-gen856']").click();
		//driver.findElementByXPath("//a[text()='Find Leads']").click();//Test Base Xpath
		driver.findElementByXPath("//ul[@class='shortcuts']/li[3]").click();//Parent to child xpath
		Thread.sleep(3000);
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("HarshaVaradhan");
		driver.findElementByXPath("(//input[@name='lastName'])[3]").sendKeys("Parameswaran");
		
		
		//driver.findElementByName("submitButton").click();a
		
		
		//driver.quit();
	}
	public static void main(String[] args) throws InterruptedException {
		BrowserTest bt = new BrowserTest();
		bt.launchBrowser();
	}

}
