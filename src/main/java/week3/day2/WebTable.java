package week3.day2;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {
	
	public void tableTrain() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("http://www.erail.in");
		driver.manage().window().maximize();//Window Maximize
		
		//Table Code
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		driver.findElementById("buttonFromTo").click();//Get Trains button
		
		Thread.sleep(3000);
		
		//Accessing Table Rows and columns
		WebElement tableTrain = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> trainRow = tableTrain.findElements(By.tagName("tr"));
		List<WebElement> trainCol = tableTrain.findElements(By.tagName("td"));
		System.out.println("Train Table Column Value: "+trainCol.get(1).getText());
		/*for(int j=0;j<trainCol.size();j++)
		{
			    String col = trainCol.get(j).getText();
			    //System.out.println(col);
		
				for(int i=0;i<trainRow.size()-1;i++)
				{
					String row = trainRow.get(i).getText();
					if(col.contains("MAS") && row.contains("MAS"))
					{					
						System.out.println(row);
					}
				}
				
		}*/
			
		
		for(int i=0;i<trainRow.size()-1;i++)
		{
			String row = trainRow.get(i).getText();
			if(row.contains("MAS"))
			{					
				System.out.println(row);
			}
		}
			
			
		}
				/*if(row.contains("CHENNAI")) {
					
					System.out.println(row);
					
				}*
		}
		
			
			//List<WebElement> tCol = trainRow.get(i).findElements(By.tagName("td"));
			//String text = tCol.get(1).getText();
			//System.out.println(text);
			
		}
		
				
		/*for (WebElement tRow : trainRow) {
			List<WebElement> tCol = tableTrain.findElements(By.tagName("td"));
			//System.out.println(tRow.getText());
			System.out.println(tCol.get(1));*/
	
	
		
	public static void main(String[] args) throws InterruptedException {
		WebTable wt = new WebTable();
		wt.tableTrain();
	}

}
