package IRCTCTestcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class CreateLead extends ProjectMethods {
	
	@Test
	public void createLead()
	{
		//login();
		WebElement createLead = locateElement("xPath","//a[text()='Create Lead']");
		click(createLead);
		
		WebElement compName = locateElement("createLeadForm_companyName");
		type(compName, "Cognizant");
		
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, "Karthikeyan");
		
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, "Kalahasthi");
		
		WebElement createLeadButton = locateElement("name", "submitButton");
		click(createLeadButton);
		
		WebElement requestCatalog = locateElement("linkText", "Request Catalog");
		click(requestCatalog);
		
		WebElement firstNameReqCatlg = locateElement("xPath", "(//input[@name='firstName'])[3]");
		type(firstNameReqCatlg, "Parameswaran");
		
		WebElement lastNameReqCatlg = locateElement("xPath", "(//input[@name='lastName'])[3]");
		type(lastNameReqCatlg, "Sudarsanam");
		
		WebElement addressReqCtlg = locateElement("generalAddress1");
		type(addressReqCtlg, "624, Gemini Magnolia Apartment");
		
		WebElement cityReqCtlg = locateElement("generalCity");
		type(cityReqCtlg, "Chennai");
		
		//dropdown state
		WebElement stateDD = locateElement("generalStateProvinceGeoId");
		selectDropDownUsingText(stateDD, "TN");
		
		WebElement pincodeReqCtlg = locateElement("generalPostalCode");
		type(pincodeReqCtlg, "600127");
		
		//dropdown country
		WebElement countryDD = locateElement("generalCountryGeoId");
		//WebElement countryDD = locateElement("xPath", "//select[@name='generalCountryGeoId']");
		selectDropDownUsingIndex(countryDD, 102);
		
		//WebElement reqCatlgBtn = locateElement("class", "//input[@class='smallSubmit disabled disabled']");
		//click(reqCatlgBtn);
	}

}
