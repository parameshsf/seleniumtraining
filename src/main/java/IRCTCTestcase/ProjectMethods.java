package IRCTCTestcase;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;

public class ProjectMethods extends leafTap.SeMethods {
	@BeforeMethod()
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);
		WebElement CRMLink = locateElement("linkText", "CRM/SFA");
		click(CRMLink);
		//locateElement("linkText", "");
	}
	//@AfterMethod()
	public void closeBrowsers()
	{
		closeBrowser();
	}

}
