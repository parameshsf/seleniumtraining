package week2.day2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class LearnMap {
	
	public void mapSample()
	{
		Map<String, Integer> map = new HashMap<>();
		map.put("Oppo", 12000);
		map.put("Apple", 80000);
		map.put("Samsung", 18000);
		map.put("RedMi", 10000);
		map.put("Honor", 6000);
		
		//System.out.println(map);
		int max = 0;
		
		System.out.println(map.values());
		//List<Integer> lm = new ArrayList<>();
  		
		
		for (Integer mo : map.values()) {
		if(mo > max) {
			max = mo;
		}
		}
		System.out.println(max);
		
		for (Entry<String, Integer> mapE : map.entrySet()) 
		{
			
			if(mapE.getValue()==max) 
			{
				System.out.println(mapE.getKey()+" "+mapE.getValue());
			}
							
		}
		
	}
	
	public static void main(String[] args) 
	{
		LearnMap cObj = new LearnMap();
		cObj.mapSample();
	}

}
