package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LearnList {
	
	public void listSample()
	{
		List<String> ls = new ArrayList<>();
		ls.add("Thalapathy");
		ls.add("Kaadhal");
		ls.add("Mannan");
		ls.add("Bahubali");
		ls.add("Thendral");
		ls.add("Bahubali");
		ls.add("Robo");
		ls.add("Bahubali");
		ls.add("Vaali");
		ls.add("Bahubali");
		int count=0;
		
		for (String movieList : ls) {
			
			if(movieList.matches("Bahubali"))
			{
				count = count + 1;
			}
			
		}
		System.out.println("The count of the duplicate movie is "+count);
		
	}
	
	public void listNumber() {
		List<Integer> lNum = new ArrayList<>(); 
		lNum.add(5);
		lNum.add(10);
		lNum.add(20);
		System.out.println("Before Reverse :"+lNum);
		System.out.println(lNum.size());
		
		System.out.println("After Reverse "+"\n");
		for(int i=lNum.size()-1;i>=0;i--)
		{
			System.out.println(lNum.get(i));
		}
	}
	
	public void setNumber()
	{
		//To display the odd index number as output
		Set<Integer> sNum = new LinkedHashSet<>();
		sNum.add(1);
		sNum.add(2);
		sNum.add(3);
		sNum.add(4);
		sNum.add(5);
		System.out.println(sNum);
		
		List<Integer> gSet = new ArrayList<>(sNum);
		for(int i=0;i<gSet.size();i++)
		{
			if(i%2!=0) {
				System.out.println(gSet.get(i));
			}
		}
	}
	
		
	public static void main(String[] args) {
		LearnList lst = new LearnList();
		//lst.listSample();
		//lst.listNumber();
		lst.setNumber();
	}

}
