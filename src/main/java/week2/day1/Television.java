package week2.day1;
//Parent Class
public class Television implements Entertainment {
	public void tvType()
	{
		System.out.println("This is a LED TV");
	}
	public void tvColor()
	{
		System.out.println("Black Color TV");
	}
	public void tvBrand()
	{
		System.out.println("Brand of the TV is L.G");
	}
	@Override
	public void tvStandards() {
		// TODO Auto-generated method stub
		System.out.println("TV Panel shoud follow all the quality process per industry standards");
		
	}
	@Override
	public void tvBasicFeatures() {
		// TODO Auto-generated method stub
		System.out.println("Per industry standards, TV should have the basic features like Remote, WIFI enabled, Surround Sound, Gaming Options, 4K color option");
		
	}
	@Override
	public void tvWarranty() {
		// TODO Auto-generated method stub
		System.out.println("All TVs manufactured in India should provide 5 years warranty to customers");
		
	}

}
