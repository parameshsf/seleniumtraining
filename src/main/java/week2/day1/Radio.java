package week2.day1;

public abstract class Radio {
	
	public void playStation() {
		System.out.println("THis is a PlayStation mention under Radio Class");
	}
	
	public abstract void changeBattery();
	
	/*public static void main(String[] args) {
		//Radio r1 = new Radio(); //This will throw an error as java does not allow to create objects for an abstract class
	}*/

}
