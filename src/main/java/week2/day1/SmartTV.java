package week2.day1;

public class SmartTV extends Television {
	public void tvPrice()
	{
		System.out.println("The price of the TV is Rs.60000");
	}
	public void tvChannels()
	{
		System.out.println("This TV can display up to 100 channels");
	}
	
	
	public static void main(String[] args) {
		
		Television tv = new Television();//Creating Television Class Object
		// Parent Class object will display Parent Class methods along with Interface methods
		SmartTV stv = new SmartTV(); //Creating SmartTV class Object
		// Child Class methods will display Child Class methods along with Parent Class methods with Interface methods
		Television pol = new SmartTV(); //Polymorphic Object
		// Polymorphic object will dislay the common methods from Parent class and child class along with unique parent class methods
		Entertainment et = new SmartTV(); //Interface object using child class
		//Interface object will display only the interface methods
		
	   
	}

}
