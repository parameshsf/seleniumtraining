package week2.day1;

public interface Entertainment {
	public void tvStandards();
	public void tvBasicFeatures();
	public void tvWarranty();

}
