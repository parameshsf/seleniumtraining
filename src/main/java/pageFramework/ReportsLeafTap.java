package pageFramework;

import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public abstract class ReportsLeafTap {
	String img1 ="";
	public static ExtentHtmlReporter objHtml;
	public static ExtentReports objEr;
	public static ExtentTest objEtest;
	public String testCaseName, testDesc, author, category;
	
	@BeforeSuite
	//@BeforeTest
	public void startReport()
	{
		objHtml = new ExtentHtmlReporter("./snapshot/ReportLead.html");//creates object for report html
		objHtml.setAppendExisting(true);//this will allow to keep history of reports
		
		objEr = new ExtentReports();
		objEr.attachReporter(objHtml);//makes the created report file editable
	}
	
	//@BeforeMethod
	//@Test
	public void assignTestCase()
	{
		objEtest = objEr.createTest(testCaseName, testDesc);
		objEtest.assignAuthor(author);
		objEtest.assignCategory(category);
		
	}	 
	
	//Method to capture the result from each function
	//@Test
	public void reportStep(String status, String desc)  {
		try {
		if (status.equalsIgnoreCase("pass")) {
				objEtest.log(Status.PASS, desc, MediaEntityBuilder.createScreenCaptureFromPath("./snapshot/img1.html").build());
			
		} else if (status.equalsIgnoreCase("fail")) {
			objEtest.log(Status.FAIL, desc, MediaEntityBuilder.createScreenCaptureFromPath("./snapshot/img1.html").build());		
		}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@AfterTest
	public void endReport()
	{
		objEr.flush();
	}
	
	

}
