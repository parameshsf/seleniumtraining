package pageFramework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;

public class HomePage extends LeafTapMethods {
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "CRM/SFA") WebElement eleCRMSFA;
	@FindBy(how = How.XPATH, using = "//a[text()='Logout']") WebElement eleLogout;
	
	@When("Click the CRMSFA link for CreateLead Verification")
	public CreateLeadPage linkCRMSFA_CreateLead()
	{
		click(eleCRMSFA);
		//return new CreateLeadPage();
		return new CreateLeadPage();
	}
	
	@When("Click the CRMSFA link for Create Case Execution")
	public CreateCasesPage linkCRMSFA_CreateCases()
	{
		click(eleCRMSFA);
		//return new CreateLeadPage();
		return new CreateCasesPage();
	}
	
	public LoginPage logout()
	{
		click(eleLogout);
		return new LoginPage();
	}

}
