package pageFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;

public class FindAccountsPage extends LeafTapMethods {
	
	
	public FindAccountsPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using="(//a[@class='linktext'])[1]") WebElement eleSearchAccount;
	Set<String> allWindows;
	//List<String> lstWindows;
	
	@And("Click the Account List displayed")
	public CreateCasesPage clickSearchAccount(){
		
			try {
				allWindows = driver.getWindowHandles();
				//lstWindows.addAll(allWindows);
				System.out.println("Before Click "+allWindows.size());
				String eleText = eleSearchAccount.getText();
				click(eleSearchAccount, eleText);
				allWindows = driver.getWindowHandles();
				System.out.println("After Click "+allWindows.size());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//driver.switchTo().window(lstWindows.get(0));
			return new CreateCasesPage();
	}

}
