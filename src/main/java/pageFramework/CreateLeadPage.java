package pageFramework;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.When;

public class CreateLeadPage extends LeafTapMethods {
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH, using = "//a[text()='Leads']") WebElement eleLeads;
	@FindBy(how = How.XPATH, using = "//a[text()='Create Lead']") WebElement eleCreateLead;
	@FindBy(how = How.ID, using ="createLeadForm_companyName") WebElement eleCompName;
	@FindBy(how = How.ID, using ="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.NAME, using="submitButton") WebElement eleCreateleadButton;
	
	@When("Click the Leads tab")
	public CreateLeadPage clickLeads()
	{
		click(eleLeads);
		return this;
	}
	
	@When("click Create Lead link")
	public CreateLeadPage clickCreateLead()
	{
		click(eleCreateLead);
		return this;
	}
	
	@When("Enter the Company Name as (.*)")
	public CreateLeadPage typeCompanyName(String companyName)
	{
		type(eleCompName,companyName );
		return this;
	}
	
	@When("Enter the First Name as (.*)")
	public CreateLeadPage typeFirstName(String firstName)
	{
		type(eleFirstName, firstName );
		return this;
	}
	
	@When("Enter the Last Name as (.*)")
	public CreateLeadPage typeLastName(String lastName)
	{
		type(eleLastName, lastName );
		return this;
	}
	
	@When("click the Create Lead Button")
	public DeleteLeadPage clickCreateLeadButton()
	{
		click(eleCreateleadButton);
		return new DeleteLeadPage();
	}

}
