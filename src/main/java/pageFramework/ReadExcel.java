package pageFramework;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {
	//@Test
	public static Object[][] readCreateLead(String dataSheetName) throws IOException
	{
		//get in to the workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		
		//get in to the sheet
		
		XSSFSheet sheet = wb.getSheetAt(0);
		
		//get the last row number to iterate in to a loop
		
		int rowCount = sheet.getLastRowNum();
		
		//get all the column number to iterate in to a loop
		
		int cellCount = sheet.getRow(1).getLastCellNum();
		Object[][] data = new Object[rowCount][cellCount];
		
		try {
			for(int i=1;i<=rowCount;i++)
			{
				XSSFRow row = sheet.getRow(i);
							     
				for(int j=0;j<cellCount;j++)
				{
					XSSFCell cell = row.getCell(j);
					String cellValue = cell.getStringCellValue();
					System.out.println(cellValue);
					data[i-1][j]=cellValue;
					
				}
			}
			wb.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	
	public static Object[][] reqCatalog() throws IOException
	{
		//get in to the workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/createLead.xlsx");
		
		//get in to the sheet
		
		XSSFSheet sheet = wb.getSheetAt(1);
		
		//get the last row number to iterate in to a loop
		
		int rowCount = sheet.getLastRowNum();
		
		//get all the column number to iterate in to a loop
		
		int cellCount = sheet.getRow(1).getLastCellNum();
		Object[][] data = new Object[rowCount][cellCount];
		
		try {
			for(int i=1;i<=rowCount;i++)
			{
				XSSFRow row = sheet.getRow(i);
							     
				for(int j=0;j<cellCount;j++)
				{
					XSSFCell cell = row.getCell(j);
					String cellValue = cell.getStringCellValue();
					System.out.println(cellValue);
					data[i-1][j]=cellValue;
					
				}
			}
			wb.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
	
	
	public static Object[][] loginLeafTap(String dataSheetName) throws IOException
	{
		//get in to the workbook
		XSSFWorkbook wb = new XSSFWorkbook("./data/"+dataSheetName+".xlsx");
		
		//get in to the sheet
		
		XSSFSheet sheet = wb.getSheetAt(0);
		
		//get the last row number to iterate in to a loop
		
		int rowCount = sheet.getLastRowNum();
		
		//get all the column number to iterate in to a loop
		
		int cellCount = sheet.getRow(1).getLastCellNum();
		Object[][] data = new Object[rowCount][cellCount];
		
		try {
			for(int i=1;i<=rowCount;i++)
			{
				XSSFRow row = sheet.getRow(i);
							     
				for(int j=0;j<cellCount;j++)
				{
					XSSFCell cell = row.getCell(j);
					String cellValue = cell.getStringCellValue();
					System.out.println(cellValue);
					data[i-1][j]=cellValue;
					
				}
			}
			wb.close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

	

}
