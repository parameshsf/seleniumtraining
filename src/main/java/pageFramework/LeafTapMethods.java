package pageFramework;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;


public class LeafTapMethods extends SeMethods {
	
	public String dataSheetName;
	
	@BeforeMethod()
	public void loginLeaftap()
	{   
		assignTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		/*WebElement loginUserName = locateElement("username");
		type(loginUserName, "demoSalesManager");
		WebElement loginPwd = locateElement("password");
		type(loginPwd, "crmsfa");
		WebElement login = locateElement("xPath", "//input[@class='decorativeSubmit']");
		click(login);
		WebElement linkCRMSFA = locateElement("linkText", "CRM/SFA");
		click(linkCRMSFA);*/
		
	}
	@AfterMethod
	public void exitBrowser()
	{
		//closeBrowser();
		closeAllBrowsers();
	}
	
	@DataProvider(name = "loginData")
	public Object[][] fetchloginData() throws IOException{
		//Object[][] loginData = ReadExcel.loginLeafTap();
		
		return ReadExcel.loginLeafTap(dataSheetName);
	}
	
	@DataProvider(name = "createLead")
	public Object[][] fetchData() throws IOException {
		//Object[][] data = ReadExcel.readCreateLead();
		return ReadExcel.readCreateLead(dataSheetName);
	}

}
