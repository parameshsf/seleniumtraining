package pageFramework;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import week4.day1.WindowHandle;

public class CreateCasesPage extends LeafTapMethods {
	
	public CreateCasesPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Cases") WebElement eleTabCases;
	@FindBy(how = How.LINK_TEXT, using = "Create Case") WebElement eleLinkCreateCase;
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[1]") WebElement eleImgInitialAccount;
	@FindBy(how = How.XPATH, using = "(//img[@alt='Lookup'])[2]") WebElement eleImgInitialContact;
	@FindBy(how = How.ID, using = "createCaseForm_custRequestCategoryId") WebElement eleDDReason;
	@FindBy(how = How.ID, using = "createCaseForm_custRequestName") WebElement eleTextSubject;
	@FindBy(how = How.XPATH, using = "//input[@class='smallSubmit']") WebElement eleButtonCreateCase;
	
	public static Set<String> allWindows;
	public static List<String> lstWindows;
	public static String parentWindow;
	
	@And("Click the Cases tab")
	public CreateCasesPage clickTabCases() {
		click(eleTabCases);
		return this;
	}
	
	@And("Click Create Case link")
	public CreateCasesPage clickLinkCreateCase() {
		click(eleLinkCreateCase);
		return this;
	}
	
	@And("Click the Initial Account icon")
	public FindAccountsPage clickInitialAccount() throws InterruptedException {
		parentWindow =  driver.getWindowHandle();
		System.out.println(parentWindow);
		click(eleImgInitialAccount);
		allWindows = driver.getWindowHandles();
		lstWindows = new ArrayList<String>();
		lstWindows.addAll(allWindows);
		System.out.println("Windows Count : "+lstWindows.size());
		driver.switchTo().window(lstWindows.get(0));
		System.out.println("1st Window Title : "+driver.getTitle());
		driver.switchTo().window(lstWindows.get(1));
		System.out.println("2nd Window Title : "+driver.getTitle());
		
		//return this;
		return new FindAccountsPage();
	}
	
	@And("Select the value from Reason DropDown")
	public CreateCasesPage selectDDReason() {
		//driver.switchTo().window(lstWindows.get(0));
		driver.switchTo().window(parentWindow);
		selectDropDownUsingText(eleDDReason, "Complex functionality");
		//driver.switchTo().window(lstWindows.get(0));
		return this;
	}
	@And("Enter the text to Subject field")
	public CreateCasesPage enterSubject() {
		type(eleTextSubject, "Testing Cases Tab");
		return this;
	}
	@And("Click the Create Case button")
	public CreateCasesPage clickButtonCreateCase() {
		
		String buttonValue = eleButtonCreateCase.getAttribute("value");
		click(eleButtonCreateCase, buttonValue);
		return this;
	}
	
}
