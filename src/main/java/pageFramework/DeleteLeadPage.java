package pageFramework;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class DeleteLeadPage extends LeafTapMethods {
	
	public DeleteLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Find Leads") WebElement eleFindLeads;
	@FindBy(how = How.XPATH, using = "(//input[@name='firstName'])[3]") WebElement eleFLFirstName;
	@FindBy(how = How.LINK_TEXT, using = "Parameswaran") WebElement eleFLLinkFirstName;
	@FindBy(how = How.XPATH, using = "//button[text()='Find Leads']") WebElement eleButtonFindLeads;
	@FindBy(how = How.XPATH, using = "//a[@class='subMenuButtonDangerous']") WebElement eleButtonDelete;
//	@FindAll(/*how = How.XPATH, using = "(//a[@class='linktext'])"*/ value = { @FindBy = "" }) WebElement verifyFirstName;
	//List<WebElement> verifyFirstName = driver.findElementsByXPath("(//a[@class='linktext'])");
		
	public DeleteLeadPage linkFindLeads()
	{
		click(eleFindLeads);
		return this;
	}
	
	public DeleteLeadPage typeFirstName()
	{
		type(eleFLFirstName, "Parameswaran");
		return this;
	}
	
	
	public DeleteLeadPage buttonFindLeads() throws InterruptedException
	{
		click(eleButtonFindLeads);
		Thread.sleep(5000);
		return this;
	}
	
	
	public DeleteLeadPage linkFLFirstName() throws InterruptedException
	{		
		
			/*for(int i=0;i<verifyFirstName.size();i++)
			{
				System.out.println(verifyFirstName.get(i).getText());
				if(verifyFirstName.get(i).getText()
						.equalsIgnoreCase("Parameswaran"))
				{
					
					System.out.println("The given string to search is : "+verifyFirstName.get(i).getText());
					click(eleFLLinkFirstName);
				}
				else
				{
					reportStep("Fail", "The given first Name is not available on the Find Leads page");
				}*/
			
		 
			click(eleFLLinkFirstName);
		 return this;
	}
	
	
	public DeleteLeadPage buttonDeleteLead()
	{
		click(eleButtonDelete);
		return this;
	}
	
	

}
