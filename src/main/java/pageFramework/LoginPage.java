package pageFramework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginPage extends LeafTapMethods {

	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.NAME, using = "USERNAME") WebElement eleUsername;
	@FindBy(how = How.NAME, using = "PASSWORD") WebElement elePassword;
	@FindBy(how = How.CLASS_NAME, using = "decorativeSubmit") WebElement eleLogin;
	
	@And("Enter the User Name as (.*)")
	public LoginPage typeUserName(String userName) 
	{
		type(eleUsername, userName);		
		return this;
	}
	
	@And("Enter the Password as (.*)")
	public LoginPage typePassword(String password)
	{
		type(elePassword, password);
		return this;
	}
	
	@When("Click the Login button")
	public HomePage clickLoginButton()
	{
		click(eleLogin);
		return new HomePage();
	}
	
	@Then("Verify if login is successful")
	public void verifyLogin() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		System.out.println("Login is Successful");
	}
	
	
	
}
