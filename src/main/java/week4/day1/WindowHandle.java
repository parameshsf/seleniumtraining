package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public void windowExercise() throws IOException
	{
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().window().maximize();//Window Maximize
		
		driver.findElementByXPath("(//span[text()=\"AGENT LOGIN\"])[1]").click();
		driver.findElementByXPath("(//a[text()=\"Contact Us\"])[1]").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lstWindows = new ArrayList<>();
		lstWindows.addAll(allWindows);
		driver.switchTo().window(lstWindows.get(1));
		String secondWindowTitle = driver.getTitle();
		System.out.println("secondWindowTitle : "+secondWindowTitle);
		//snapshot functionality
		File source = driver.getScreenshotAs(OutputType.FILE);
		File fObj = new File(".\\snapshot\\ContactUs.jpg");
		FileUtils.copyFile(source, fObj);
		if(fObj.exists())
		{
			System.out.println("File Created");
		}
		driver.switchTo().window(lstWindows.get(0));
		driver.close();
		
		//driver.close();
	}
	
	public static void main(String[] args) throws IOException {
		WindowHandle wh = new WindowHandle();
        
		wh.windowExercise();
	}
}
