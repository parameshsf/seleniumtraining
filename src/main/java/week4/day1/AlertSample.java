package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertSample {

	public void AlertExercise() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text()='Try it']").click();
		//driver.findElementByLinkText("Try it").click();
		Thread.sleep(2000);
		
		Alert tryAlert = driver.switchTo().alert();
		Thread.sleep(2000);
		tryAlert.sendKeys("Parameswaran");
		Thread.sleep(2000);
		tryAlert.accept();
		//String findText = tryAlert.getText();
		String text = driver.findElementByXPath("//p[@id=\"demo\"]").getText();
		System.out.println(text);
		
		if(text.contains("Parameswaran"))
		{
			System.out.println("Alert Text Found");
		}
	}
	
	
	public static void main(String[] args) throws InterruptedException {
		AlertSample obj = new AlertSample();
		obj.AlertExercise();
	}
	
	
}
