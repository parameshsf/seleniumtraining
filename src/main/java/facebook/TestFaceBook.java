package facebook;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class TestFaceBook extends SeMethods {
	
	
	@Test
	public void facebookTest()
	{
		startApp("chrome", "https://www.facebook.com");
		WebElement email = locateElement("email");
		type(email, "parameshsf");
		
		WebElement pwd = locateElement("pass");
		type(pwd, "iloveu2216");
		
		WebElement login = locateElement("loginbutton");
		click(login);
		
	/*	WebElement home = locateElement("linkText", "Home");
		click(home);*/
		
		WebElement search = locateElement("name", "q");
		
		type(search, "TestLeaf");
		
		WebElement searchIcon = locateElement("xPath","//button[@class='_42ft _4jy0 _4w98 _4jy3 _517h _51sy _4w97']");
	    click(searchIcon);
			        
		WebElement likeTestLeaf = locateElement("xPath", "(//button[text()='Like'])[1]");
		WebElement testLeaf = locateElement("xPath", "//div[text()='TestLeaf']");
		
		if(likeTestLeaf.getText().equalsIgnoreCase("Like"))
		{
			click(likeTestLeaf);
			click(testLeaf);
		}
		else if(likeTestLeaf.getText().equalsIgnoreCase("Liked"))
		{
			click(testLeaf);
		}
		
	}
	
}
