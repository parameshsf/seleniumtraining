package leafTap;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import week6.day2.ReadExcel;

public class TestDataProvider extends LeafTapMethods {
				
		@BeforeTest
		public void setData() {
			testCaseName = "Data Provided Sample";
			testDesc = "Create a new Lead";
			author = "Paramesh";
			category = "Functional Test";
		}

		@Test(dataProvider = "createLead")
		public void createLead(String cName,String fName,String lName,String email,String ph) {	
			click(locateElement("linkText", "Leads"));
			click(locateElement("linkText", "Create Lead"));
			type(locateElement("id", "createLeadForm_companyName"), cName);
			type(locateElement("id", "createLeadForm_firstName"), fName);
			type(locateElement("id", "createLeadForm_lastName"), lName);
			type(locateElement("id", "createLeadForm_primaryEmail"), email);
			type(locateElement("id", "createLeadForm_primaryPhoneNumber"), ""+ph);
			click(locateElement("name", "submitButton"));		
		}
		
		@DataProvider(name = "createLead")
		public Object[][] fetchData() throws IOException {
			Object[][] data = ReadExcel.readCreateLead();
			
			/*Object[][] data = new Object[2][5];
			data[0][0] = "TestLeaf"; 
			data[0][1] = "sarath";
			data[0][2] = "M";
			data[0][3] = "sarath@Testleaf.com";
			data[0][4] = 124567890;
			
			data[1][0] = "IBM";
			data[1][1] = "karthi";
			data[1][2] = "G";
			data[1][3] = "karthi@IBM.com";
			data[1][4] = 124567891;*/
			return data;
		}
		
		@Test(dataProvider = "requestCatalog")
		public void requestCatalog(String fName,String lName,String address1,String city,String zip) throws InterruptedException {	
			click(locateElement("linkText", "Leads"));
			Thread.sleep(3000);
			click(locateElement("xPath", "//a[text()='Request Catalog ']"));
			type(locateElement("xPath", "(//input[@name='firstName'])[3]"),fName);
			type(locateElement("xPath", "(//input[@name='lastName'])[3]"),lName);
			type(locateElement("id", "generalAddress1"),address1);
			type(locateElement("id","generalCity"),city);
			type(locateElement("id","generalPostalCode"),""+zip);
			
			/*WebElement stateDD = locateElement("generalStateProvinceGeoId");
			selectDropDownUsingText(stateDD, "AL");*/
			selectDropDownUsingText(locateElement("id","generalStateProvinceGeoId"), "AL");
			selectDropDownUsingText(locateElement("id","generalCountryGeoId"), "United States");
			
		}
		
		@DataProvider(name = "requestCatalog")
		public Object[][] fetchReqCatalog() throws IOException{
			Object[][] reqCatalogData = ReadExcel.reqCatalog();
			
			return reqCatalogData;
			
		}
		
		
		
}


