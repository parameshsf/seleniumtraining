package leafTap;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateLead extends LeafTapMethods {
	@BeforeClass
	public void setData()
	{
		testCaseName = "CreateLead";
		testDesc = "Create A new Lead";
		author = "Parameswaran Sudarsanam";
		category = "Regression Test";
	}
	
		
	@Test
	public void createLeadPage()
	{
				
		WebElement leadsLink = locateElement("linkText", "Leads");
		click(leadsLink);
		
		WebElement createLeadLink = locateElement("linkText", "Create Lead");
		click(createLeadLink);
		
		WebElement companyName = locateElement("createLeadForm_companyName");
		type(companyName, "Cognizant");
		
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, "Parameswaran");
		
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, "Sudarsanam");
		
		WebElement createLeadButton = locateElement("xPath", "//input[@name='submitButton']");
		click(createLeadButton);
	}
	
	public Object[][] inputData()
	{
		
		return null;
		
	}

}
