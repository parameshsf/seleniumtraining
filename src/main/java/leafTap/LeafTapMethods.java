package leafTap;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;


public class LeafTapMethods extends SeMethods {
	@BeforeMethod()
	public void loginLeaftap()
	{   
		assignTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement loginUserName = locateElement("username");
		type(loginUserName, "demoSalesManager");
		WebElement loginPwd = locateElement("password");
		type(loginPwd, "crmsfa");
		WebElement login = locateElement("xPath", "//input[@class='decorativeSubmit']");
		click(login);
		WebElement linkCRMSFA = locateElement("linkText", "CRM/SFA");
		click(linkCRMSFA);
		
	}
	@AfterMethod
	public void exitBrowser()
	{
		closeBrowser();
	}

}
