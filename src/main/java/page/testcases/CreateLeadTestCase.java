package page.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pageFramework.LeafTapMethods;
import pageFramework.LoginPage;

public class CreateLeadTestCase extends LeafTapMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "Create Lead TestCase";
		testDesc = "Create Lead";
		author = "Paramesh";
		category = "Smoke Test";
		//dataSheetName = "loginLeafTap";
		dataSheetName = "createLead";
	}

	
	@Test(dataProvider = "createLead")
	public void createLead(String uName,String pwd, String compName, String firstName, String lastName)
	{
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pwd)
		.clickLoginButton()
		.linkCRMSFA_CreateLead()
		.clickLeads()
		.clickCreateLead()
		.typeCompanyName(compName)
		.typeFirstName(firstName)
		.typeLastName(lastName)
		.clickCreateLeadButton();
		
	}
	
	/*@DataProvider(name = "createLead")
	public Object[][] fetchData() throws IOException {
		Object[][] data = ReadExcel.readCreateLead();
		return data;
	}*/
	
	
	
	

}
