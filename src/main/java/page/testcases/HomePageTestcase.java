package page.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pageFramework.HomePage;
import pageFramework.LeafTapMethods;
import pageFramework.LoginPage;

public class HomePageTestcase extends LeafTapMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "Page Model TestCase";
		testDesc = "LeafTap Home Page";
		author = "Paramesh";
		category = "Smoke Test";
		dataSheetName = "loginLeafTap";
	}
	
		
	@Test(dataProvider = "loginData")
	public void loginLeafTap(String uName, String pwd)
	{
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pwd)
		.clickLoginButton()
		.linkCRMSFA_CreateLead();
	}

}
