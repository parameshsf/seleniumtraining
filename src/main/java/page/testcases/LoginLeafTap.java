package page.testcases;

import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageFramework.LeafTapMethods;
import pageFramework.LoginPage;
import pageFramework.ReadExcel;

public class LoginLeafTap extends LeafTapMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "Page Model TestCase";
		testDesc = "Login Leaf Tap";
		author = "Paramesh";
		category = "Smoke Test";
		dataSheetName = "loginLeafTap";
	}
	
		
	@Test(dataProvider = "loginData")
	public void loginLeafTap(String uName, String pwd)
	{
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pwd)
		.clickLoginButton();
		
	}
	
	/*@DataProvider(name = "loginData")
	public Object[][] fetchloginData() throws IOException{
		Object[][] loginData = ReadExcel.loginLeafTap();
		
		return loginData;
	}*/
	
	
	

}
