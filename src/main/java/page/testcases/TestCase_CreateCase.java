package page.testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pageFramework.HomePage;
import pageFramework.LeafTapMethods;
import pageFramework.LoginPage;

public class TestCase_CreateCase extends LeafTapMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "Page Model TestCase";
		testDesc = "LeafTap Home Page";
		author = "Paramesh";
		category = "Smoke Test";
		dataSheetName = "loginLeafTap";
	}
	
		
	@Test(dataProvider = "loginData")
	public void createCases(String uName, String pwd) throws InterruptedException
	{
		new LoginPage()
		.typeUserName(uName)
		.typePassword(pwd)
		.clickLoginButton()
		.linkCRMSFA_CreateCases()
		.clickTabCases()
		.clickLinkCreateCase()
		.clickInitialAccount()
		.clickSearchAccount()
		.selectDDReason()
		.enterSubject()
		.clickButtonCreateCase();
		
	}

}
