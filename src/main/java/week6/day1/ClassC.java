package week6.day1;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import leafTap.SeMethods;

public class ClassC extends SeMethods {
	
	//@Test(groups= {"Regression"},dependsOnGroups= {"sanity"})
	@Test(groups= {"Regression"})
	public void testGoogle()
	{
		startApp("chrome", "https://www.google.com");
		WebElement searchGoogle = locateElement("name", "q");
		type(searchGoogle, "TestLeaf");
		searchGoogle.sendKeys(Keys.ENTER);
	}

}
