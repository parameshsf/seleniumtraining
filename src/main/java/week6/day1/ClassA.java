package week6.day1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import zoomcar.project.SeMethods;

//ZoomCar
public class ClassA extends SeMethods {
	
	List<Integer> getPrice = new ArrayList<Integer>();
	@Test(invocationCount=1, timeOut = 15000, enabled=true,groups={"smoke"})
	public void zoomcarLogin()
	{
		startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement zoomStart = locateElement("linkText", "Start your wonderful journey");
		click(zoomStart);
		WebElement pickUpPoint = locateElement("xPath", "(//div[@class='items'])[3]");
		click(pickUpPoint);
		WebElement nextBtn = locateElement("xPath", "//button[text()='Next']");
		click(nextBtn);
		datePicker();
		//WebElement nextDay = locateElement("xPath","//div[@class=\"day picked \"]/following::div[1]");
		WebElement nextDay = locateElement("xPath","//div[@class='day'][contains(text(),'"+datePicker()+"')]");
		click(nextDay);
		click(nextBtn);
		WebElement doneBtn = locateElement("xPath","//button[text()='Done']");
		click(doneBtn);
	    List<WebElement> carPrice = driver.findElementsByXPath("//div[@class='price']");
	    
	    for(int i=0;i<carPrice.size();i++)
	    {
	    	//System.out.println(carPrice.get(i).getText());
	    	String prices = carPrice.get(i).getText().replaceAll("[^0-9]", "").trim();
	    	getPrice.add(Integer.parseInt(prices));
	    	
	    }
	    
	    Collections.sort(getPrice);
	    int lowPrice = getPrice.get(0);
	    int size = getPrice.size();
	    int highPrice = getPrice.get(size-1);
	    System.out.println("The lowest Car Price is : "+lowPrice);
	    System.out.println("Highest Car price : " +getPrice.get(size-1));
	    //String highCarPrice = driver.findElementByXPath("//div[contains(text(),'"+highPrice+"')]//parent::*//parent::*//parent::*//div[@class='details']/h3").getText();
	    String highCarName = driver.findElementByXPath("//div[contains(text(),'"+highPrice+"')]/../../..//h3").getText();
		System.out.println("The costliest car price is :"+highCarName);
	}
	
	public Integer datePicker()
	{
		Date whenDate = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String currentDate = sdf.format(whenDate);
		System.out.println(currentDate);
		int nextDate = Integer.parseInt(currentDate)+1;
		return nextDate;
		//System.out.println("Next Date "+nextDate);
	}
	

}
