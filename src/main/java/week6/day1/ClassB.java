package week6.day1;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import leafTap.SeMethods;

//LeafTap
public class ClassB extends SeMethods {
	//@Test(dependsOnMethods=("week6.day1.ClassA.zoomcarLogin"),alwaysRun=true,groups= {"sanity"},dependsOnGroups= {"smoke"})
	@Test(dependsOnMethods=("week6.day1.ClassA.zoomcarLogin"),alwaysRun=true,groups= {"sanity"})
	public void createLeadPage()
	{
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement loginUserName = locateElement("username");
		type(loginUserName, "demoSalesManager");
		WebElement loginPwd = locateElement("password");
		type(loginPwd, "crmsfa");
		WebElement login = locateElement("xPath", "//input[@class='decorativeSubmit']");
		click(login);
		WebElement linkCRMSFA = locateElement("linkText", "CRM/SFA");
		click(linkCRMSFA);
		
		WebElement leadsLink = locateElement("linkText", "Leads");
		click(leadsLink);
		
		WebElement createLeadLink = locateElement("linkText", "Create Lead");
		click(createLeadLink);
		
		WebElement companyName = locateElement("createLeadForm_companyName");
		type(companyName, "Cognizant");
		
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, "Parameswaran");
		
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, "Sudarsanam");
		
		WebElement createLeadButton = locateElement("xPath", "//input[@name='submitButton']");
		click(createLeadButton);
	}

}
