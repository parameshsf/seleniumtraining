package week6.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeMethod;

import leafTap.SeMethods;


public class LeafTapMethods extends SeMethods {
	@BeforeMethod()
	public void loginLeaftap()
	{   
		assignTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement loginUserName = locateElement("username");
		type(loginUserName, "demoSalesManager");
		WebElement loginPwd = locateElement("password");
		type(loginPwd, "crmsfa");
		WebElement login = locateElement("xPath", "//input[@class='decorativeSubmit']");
		click(login);
		WebElement linkCRMSFA = locateElement("linkText", "CRM/SFA");
		click(linkCRMSFA);
		
	}

}
