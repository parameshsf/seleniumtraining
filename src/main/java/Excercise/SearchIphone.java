package Excercise;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SearchIphone {
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.get("https://www.flipkart.com/");
		driver.getKeyboard().sendKeys(Keys.ESCAPE);
		driver.manage().window().maximize();//Window Maximize
		
		driver.findElementByXPath("//input[@name='q']").sendKeys("iPhone X",Keys.ENTER);
		//driver.findElementByXPath("//button[@class='vh79eN']").click();
		
		String value1 = driver.findElementByXPath("(//div[@class='_3auQ3N'])[2]").getText().replaceAll("[^0-9]", "");
		int phoneOrgPrice = Integer.parseInt(value1);
		
	    String value2 = driver.findElementByXPath("(//div[@class='_1vC4OE'])[2]").getText().replaceAll("[^0-9]", "");
	    int phoneOfferPrice = Integer.parseInt(value2);
		System.out.println(value1);
		System.out.println("Phone Original Price is Rs. "+phoneOrgPrice);
		System.out.println("Phone offer price is Rs. "+phoneOfferPrice);
		
		if(phoneOfferPrice<phoneOrgPrice)
		{
			int phoneDiscount = (phoneOrgPrice - phoneOfferPrice);
			System.out.println("The discount price of iphone is Rs"+phoneDiscount);
		}
		
		
		
	}

}
