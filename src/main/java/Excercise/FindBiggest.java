package Excercise;

public class FindBiggest {
	
	public void swapNumber() {
		int num1 = 10, num2 = 20;
		if(num1!=num2)
		{
			num1 = num1 + num2;
			num2 = num1 - num2;
			num1 = num1 - num2;
			System.out.println("num1 :"+num1);
			System.out.println("num2 :"+num2);
		}
		else
		{
			System.out.println("Both the numbers are same. Please use different numbers to swap.");
		}
		
		
	}
	
	public static void main(String[] args) {
		int big[] = {20,340,21,879,92,21,474,83647};
		int big1 = 0, big2 = 0;
		for(int i=0;i<big.length;i++)
		{
			if(big[i]>big1) {
				big2 = big1;
				big1 = big[i];
			}
			else if(big[i]>big2)
			{
				big2 = big[i];
			}
		}
		System.out.println("The second largest number from an array is : "+big2);
		FindBiggest fb = new FindBiggest();
		fb.swapNumber();
			
	}

}
