package Excercise;

import java.util.Scanner;

public class SwitchSample {
	public void switchMaths()
	{
		
		try {
			Scanner obj = new Scanner(System.in);
						
			System.out.println("This program performs given arithmatic operations on the entered two numbers"+"\n"+"\n");
			System.out.println("Enter the first number:"+"\n");
			int n1=obj.nextInt();
			System.out.println("Enter the second number:"+"\n");
			int n2=obj.nextInt();
			System.out.println("Enter one of the arithmatic operation to be performed as Add/Mul/Div/Sub"+"\n");
			String opr =obj.next();
			
			switch (opr) 
			{
			case "Add":
				int sum = n1+n2;
				System.out.println("The addtion of two numbers are :"+sum);
				break;
			case "Sub":
				int bal = n1-n2;
				System.out.println("The subtraction of two numbers are :"+bal);
				break;
			case "Mul":
				double multiply = n1*n2;
				System.out.println("The multiplication of two numbers are :"+multiply);
				break;
			case "Div":
				float divide = n1/n2;
				System.out.println("The division of two numbers are :"+divide);
			default:
				break;
			}
			obj.close();
			
		} 
		catch (ArithmeticException e) {
			System.out.println("Invalid Arithmatic Value Entered");
			// TODO: handle exception
		}
		catch (Exception e) {
			System.out.println("Please enter valid arithmatic entries");
		}
	}
	
	public static void main(String[] args) {
		SwitchSample sw = new SwitchSample();
		sw.switchMaths();
	}
	
	
}


