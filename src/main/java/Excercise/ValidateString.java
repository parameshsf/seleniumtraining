package Excercise;

public class ValidateString {
	
	public static void main(String[] args) {
		String val = "Tiger Runs @ The Speed Of 100 Km Per Hour";
		System.out.println("Count of letters in a string : "+val.length());
		
		int upperCase = 0, lowerCase = 0, space = 0, specialChar = 0, num=0;
		char str[] = val.toCharArray();
		
		for(int i=0;i<val.length();i++)
		{
			char charAt = str[i];
			
			if(Character.isWhitespace(charAt))
			{
				space++;
			}
			else if(Character.isDigit(charAt))
			{
				
				num++;
			}
			else if(Character.isLowerCase(charAt))
			{
				lowerCase++;
			}
			else if(Character.isUpperCase(charAt))
			{
				upperCase++;
			}
			else
			{
				specialChar++;
			}
	}
		float uCasePercentage = (upperCase*100)/val.length();
		float lCasePercentage = (lowerCase*100)/val.length();
		float spacePercentage = (space*100)/val.length();
		float numberPercentage = (num*100)/val.length();
		float specialCharPercent = (specialChar*100)/val.length();
		
		System.out.println("Count of Upper Case in String is "+upperCase+" % of UpperCase letter in a String is : "+ uCasePercentage);
		System.out.println("Count of Lower Case in String is "+lowerCase+" % of lowerCase letter in a String is : "+ lCasePercentage);
		System.out.println("Count of space in String is "+space+" % of space in a String is : "+ spacePercentage);
		System.out.println("Count of number in String is "+num+" % of number in a String is : "+ numberPercentage);
		System.out.println("Count of Special Character in String is "+specialChar+" % of Special Character in a String is : "+ specialCharPercent);
	}
}
	


