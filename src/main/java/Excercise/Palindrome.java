package Excercise;

import java.util.Scanner;

public class Palindrome {
	
	public void verifyPalindrome()
	{
		try {
			
			System.out.println("This program is to verify whether the given string is Palindrome or not.");
			
			Scanner obj = new Scanner(System.in);
			System.out.println("Enter the string: ");
			String str1 = obj.next();
			System.out.println(str1.length());
			
			String reverse = "";
			
			for(int i=str1.length()-1;i>=0;i--)
			{
				reverse = reverse + str1.charAt(i);
			}
			System.out.println("Reverse : "+reverse);
			if(str1.equalsIgnoreCase(reverse))
			{
				System.out.println("The given string "+str1+" is Palindrome ");
			}
			else
			{
				System.out.println("The given string "+str1+" is not Palindrome");
			}
			
		} catch (StringIndexOutOfBoundsException e) {
			// TODO: handle exception
			System.out.println("Invalid input given: "+e.getMessage());
		}
		
		
	}
	
	public static void main(String[] args) {
		
		Palindrome pal = new Palindrome();
	    pal.verifyPalindrome();
	}

}
