package Excercise;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExp {
	
	public static void main(String[] args) {
		Scanner str = new Scanner(System.in);
		System.out.println("Enter the password to validate");
		String password = str.next();
		
		String pwdPattern = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d$!@#%&*?]{8,10}$";
		
		Pattern comp = Pattern.compile(pwdPattern);
		Matcher match = comp.matcher(password);
					
		System.out.println(match.matches());
	}

}
