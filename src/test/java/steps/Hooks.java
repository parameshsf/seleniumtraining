package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import pageFramework.SeMethods;

public class Hooks extends SeMethods {
	@Before
	public void beforeRun(Scenario sc)
	{
		startReport();
		testCaseName = sc.getName();
		testDesc = sc.getId();
		author = "hi";
		category = "smoke";
		assignTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		//System.out.println("The Scenario Name : "+sc.getName());
	}
	@After
	public void afterRun(Scenario sc)
	{
		//System.out.println("The Data ID used in the sceanario : "+sc.getId());
		System.out.println("The status of the scenario executed : "+sc.getStatus());
		closeBrowser();
		endReport();
	}

}
