/*package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginLeafTap {
	
	public ChromeDriver driver;
	
	@Given("Launch the Chrome Browser")
	public void launchBrowser()
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.MILLISECONDS);
		
	}
	
	@And("Enter the URL")
	public void enterURL() {
	    // Write code here that turns the phrase above into concrete actions
		driver.get("http://leaftaps.com/opentaps/control/main");
	    //throw new PendingException();
	}

	@And("Enter the User Name as (.*)")
	public void enterUserName(String username) {
		 
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.findElementById("username").sendKeys(username);
		
	}

	@And("Enter the Password as (.*)")
	public void enterPassword(String pwd) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.findElementById("password").sendKeys(pwd);
	}

	@When("Click the Login button")
	public void clickLoginButton() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.findElementByClassName("decorativeSubmit").click();
		}

	@Then("verify if login is successful")
	public void verifyLogin() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		System.out.println("Login is Successful");
	}
	
	@When("Click the CRMSFA link")
	public void clickCRMSFALink() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		WebElement crmsfaLink = driver.findElementByLinkText("CRM/SFA");
		crmsfaLink.click();
	}

	@When("Click the Leads tab")
	public void clickLeadsTab() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		WebElement leadsTab = driver.findElementByXPath("//a[text()='Leads']");
		leadsTab.click();
	}

	@When("click Create Lead link")
	public void clickCreateLeadLink() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		WebElement createLeadLink = driver.findElementByXPath("//a[text()='Create Lead']");
		createLeadLink.click();
	}

	@When("Enter the Company Name as (.*)")
	public void enterCompName(String compName) {
	    // Write code here that turns the phrase above into concrete actions
		driver.findElementById("createLeadForm_companyName").sendKeys(compName);
	    
	}

	@When("Enter the First Name as (.*)")
	public void enterTheFirstNameAsFirstName(String fName) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@When("Enter the Last Name as (.*)")
	public void enterTheLastNameAsLastName(String lName) {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("click the Create Lead Button")
	public void clickTheCreateLeadButton() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		WebElement createLeadButton = driver.findElementByXPath("//input[@class='smallSubmit']");
		createLeadButton.click();
	}

	@Given("Enter the User Name as DemoCSR")
	public void enterTheUserNameAsDemoCSR() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("Enter the User Name as Demo")
	public void enterTheUserNameAsDemo() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

	@Given("Enter the Password as error")
	public void enterThePasswordAsError() {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
	}

}
*/