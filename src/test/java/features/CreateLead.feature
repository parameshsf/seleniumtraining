Feature: Login leaftaps
#Background:
#Given Launch the Chrome Browser
#And Enter the URL
@smoke
Scenario Outline: Positive Login
And Enter the User Name as <username>
And Enter the Password as <password>
When Click the Login button
Then Verify if login is successful
When Click the CRMSFA link for CreateLead Verification
When Click the Leads tab
When click Create Lead link
And Enter the Company Name as <comp name>
And Enter the First Name as <first name>
And Enter the Last Name as <last name>
When click the Create Lead Button
Examples:
	|username|password|comp name|first name|last name|
	|DemoSalesManager|crmsfa|Cognizant|Parameswaran|Sudarsanam|
	|DemoCSR|crmsfa|TCS|Anand|Karunakaran|
	
@RiskBasedTesting
Scenario Outline: Negative Login
And Enter the User Name as <username>
And Enter the Password as <password>
When Click the Login button
Then Verify if login is successful
Examples:
	|username|password|
	|Demo|error|