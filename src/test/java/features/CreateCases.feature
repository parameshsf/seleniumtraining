Feature: Verify CreateCase 
#Background:
#Given Launch the Chrome Browser
#And Enter the URL
@smoke
Scenario Outline: Positive Login
And Enter the User Name as <username>
And Enter the Password as <password>
When Click the Login button
Then Verify if login is successful
When Click the CRMSFA link for Create Case Execution
And Click the Cases tab
And Click Create Case link
And Click the Initial Account icon
And Click the Account List displayed
And Select the value from Reason DropDown
And Enter the text to Subject field
And Click the Create Case button
#And Enter the Company Name as <comp name>
#And Enter the First Name as <first name>
#And Enter the Last Name as <last name>
#When click the Create Lead Button
Examples:
	|username|password|
	|DemoSalesManager|crmsfa|
	|DemoCSR|crmsfa|
	
@RiskBasedTesting
Scenario Outline: Negative Login
And Enter the User Name as <username>
And Enter the Password as <password>
When Click the Login button
Then Verify if login is successful
Examples:
	|username|password|
	|Demo|error|