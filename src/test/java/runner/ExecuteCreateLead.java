package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
/*import cucumber.api.SnippetType;*/
//import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		//features = "src/test/java/features/", -> This line will execute all the feature files in the path
		features = "src/test/java/features/CreateLead.feature",
		glue = {"steps", "pageFramework"},
		monochrome = true,
		tags = "@smoke"
		//dryRun = true,
		//snippets = SnippetType.CAMELCASE
		)


public class ExecuteCreateLead {

}
